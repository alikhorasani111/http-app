import {useEffect, useState} from "react";
import '../App.css'
import getOneComment from "../services/GetOneComment";

const FullComment = ({commentId, deletedHandler}) => {
    const [comments, setComments] = useState(null);

    useEffect(() => {
        if (commentId) {
            getOneComment(commentId).then((res) => {
                setComments(res.data);
            }).catch(() => {});
        }
    }, [commentId]);


    let commentDetail = <h3>Please selected comment</h3>;
    if (commentId) commentDetail = <p>Loading...</p>;
    if (comments && commentId) commentDetail = (
        <div className="fullComment">
            <p>{comments.name}</p>
            <p>{comments.email}</p>
            <p>{comments.body}</p>
            <button onClick={() => deletedHandler(commentId)} className="btn_style">Deleted</button>
        </div>);
    return commentDetail;
};
export default FullComment;