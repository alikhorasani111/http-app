import React from "react";
import {NavLink} from 'react-router-dom'

const Navigation = () => {
    return (
        <header>
            <nav className="nav">
                <ul>
                    <li><NavLink className={(navData) => navData.isActive ? "red" : "black"} to="/">Comments</NavLink></li>
                    <li><NavLink className={(navData) => navData.isActive ? "red" : "black"} to="/newComment">New Comments</NavLink></li>
                </ul>
            </nav>
        </header>
    )
};
export default Navigation;
