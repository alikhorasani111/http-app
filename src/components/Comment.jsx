import '../App.css'
import {useEffect, useState} from "react";
import getAllComments from "../services/GetAllComments";
import {toast} from "react-toastify";
import FullComment from "./FullCommenr";
import DeletedComment from "../services/DeletedComment";

const Comment = () => {
    const [comments, setComments] = useState(null);
    const [error, setError] = useState(false);
    const [selectedId, setSelectedId] = useState(null);

    useEffect(() => {
        const getComments = async () => {
            try {
                const {data} = await getAllComments();
                setComments(data.slice(0, 10))
            } catch (error) {
                setError(true)
            }
        };
        getComments();
    }, []);
    const selectedCommentHandler = (id) => {
        setSelectedId(id);
    };
    const deletedHandler = async (selectedId) => {
        try {
            await DeletedComment(selectedId);
            const {data} = await getAllComments();
            setComments(data);
            setSelectedId(null);

        } catch (e) {

        }
    };

    const RenderComments = () => {
        let renderedValue =
            <div>
                <p>Loading ...</p>
            </div>;

        if (error) {
            renderedValue = <p>fetching data failed !!!</p>;
            toast.error('there is an error');
        }

        if (comments && !error) {

            renderedValue = comments.map((c) =>
                <div key={c.id} onClick={() => selectedCommentHandler(c.id)} className="border">
                    <p>name : {c.name}</p>
                    <p>email : {c.email}</p>
                </div>
            );


        }

        return renderedValue;
    };


    return (
        <>
            <div className="d-flex">
                {RenderComments()}
            </div>

            <FullComment deletedHandler={deletedHandler} commentId={selectedId}/>

        </>

    )
};
export default Comment;