import {useState} from "react";
import '../App.css'
import {useNavigate} from 'react-router-dom'
import {toast} from 'react-toastify'

import getNewComment from '../services/GetNewComment';
import getAllComments from "../services/GetAllComments";

const NewComment = ({setComments}) => {

    const navigate = useNavigate();

    const [userValue, setUserValue] = useState({
        name: "",
        email: "",
        body: ""
    });

    const changeHandler = (e) => {
        setUserValue({...userValue, [e.target.name]: e.target.value})
    };


    const postHandler = async (e) => {
        try {
            e.preventDefault();
            await getNewComment({...userValue, postId: 10});
            const {data} = await getAllComments();
            setComments(data);
        }
        catch (e) {

        }
        toast.success("your comment add to list");
        navigate("/")

    };

    return (
        <div className="fullComment">
            <form>
                <div className="form_style">
                    <label htmlFor="name">name :</label>
                    <input onChange={(e) => changeHandler(e)} type="text" id="name" name="name"
                           value={userValue.name}
                           placeholder="please add your name"
                    />
                </div>

                <div className="form_style">
                    <label htmlFor="email">email :</label>
                    <input onChange={(e) => changeHandler(e)} type="text" id="email" name="email"
                           value={userValue.email}
                           placeholder="please add your email"
                    />
                </div>
                <div className="form_style">
                    <label htmlFor="body">body :</label>
                    <textarea onChange={(e) => changeHandler(e)} name="body" id="body" value={userValue.body}
                              rows="5"
                              placeholder="please add your description"/>
                </div>

                <button onClick={postHandler} type="submit" className="btn_style">add form</button>
            </form>
        </div>
    )
};
export default NewComment;