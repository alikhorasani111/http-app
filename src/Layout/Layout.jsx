import Navigation from "../components/Navigation";
import React from "react";
import '../App.css'
const Layout = ({children}) => {
    return (
        <>
            <Navigation/>
            {children}
            <footer className="footer">footer</footer>
        </>
    )
};
export default Layout