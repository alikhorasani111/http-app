import axios from 'axios';

axios.defaults.baseURL = "http://localhost:3001";
//axios.defaults.headers.common['Authorization'] = 'AUTH_TOKEN';

axios.interceptors.request.use(
    (request) => {
        console.log(request);
        return request;
    },
    (error) => {
        return Promise.reject();
        console.log(error);
    }
);

axios.interceptors.response.use(
    (response) => {
        console.log(response);
        return response;
    },
    (error) => {
        return Promise.reject();
        console.log(error);
    }
);


const http = {
    get: axios.get,
    put: axios.put,
    post: axios.post,
    delete: axios.delete
};

export default http;