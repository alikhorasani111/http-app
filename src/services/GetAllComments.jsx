import http from './HttpService';


const getAllComments = () => {
    return  http.get('/comments')
};

export default getAllComments;