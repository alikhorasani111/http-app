import http from './HttpService';

const getNewComment = (data) => {
    const token='SECURE TOKEN !';

    return http.post('/comments', data, {
        headers: {
            Authorization:`Bearer ${token}`
        }
    });
};

export default getNewComment;