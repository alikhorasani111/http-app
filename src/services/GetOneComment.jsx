import http from './HttpService';


const getOneComment=(commentId)=>{
   return  http.get(`/comments/${commentId}`)
};

export default getOneComment;