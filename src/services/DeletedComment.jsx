import http from './HttpService';


const DeletedComment = (selectedId) => {
    return  http.delete(`/comments/${selectedId}`)
};

export default DeletedComment;