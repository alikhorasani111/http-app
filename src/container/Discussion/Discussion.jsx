import Comment from "../../components/Comment";
import '../../App.css'
import {Routes, Route} from 'react-router-dom'
import Layout from "../../Layout/Layout";
import React from "react";
import NewComment from "../../components/NewComment";

const Discussion = () => {

    return (

        <Layout>
            <Routes>
                <Route path="/" element={<Comment/>}/>
                <Route path="/newComment" element={<NewComment/>}/>
            </Routes>
        </Layout>
    )
};
export default Discussion